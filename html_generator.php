<?php
//function to generate html file
function html_generator($insertion_id, $html_columns){
  if (!file_exists('generated_files')) {
  mkdir('generated_files', 0777, true);
  }
  //create html file with last 5
  $file_name = "generated_files/calc_".$insertion_id.".html"; // or .php
   $fh = fopen($file_name, 'w'); // or die("error");
   $stringData = '<!DOCTYPE html>
   <html lang="en" dir="ltr">
     <head>
     <link rel="stylesheet" href="../css/style.css">
     <link rel="stylesheet" href="../css/bootstrap.min.css">
       <meta charset="utf-8">
       <title></title>
     </head>
     <body>
     <div class="container">
       <table class="col-md-12 text-center table table-striped align-middle mt-5" border="1">
         <thead class="thead-dark">
           <th>ID</th>
           <th>First no</th>
           <th>Second no</th>
           <th>Average</th>
           <th>Area</th>
           <th>Area ^ 2</th>
         </thead>
         <tbody>'
         .$html_columns.
         '</tbody>
       </table>
       </div>
     </body>
   </html>';
   fwrite($fh, $stringData);
   fclose($fh);
   return "you can find html file from here => '".$file_name . "'\n";
}
