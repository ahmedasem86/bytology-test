<?php
$html_columns = "";

$average = avg($inputs);
$area = area($inputs[0] , $inputs[1]);
$area_square = pow($area , 2);

// Insert record into db
$sql_insert = "INSERT INTO log_requests (integer_1, integer_2, average , area , area_square)VALUES ('$inputs[0]', '$inputs[1]', '$average','$area','$area_square')";
if ($conn->query($sql_insert) === TRUE) {
  $insertion_id = $conn->insert_id;
  echo "Insertion succeeded below are the latest requests \n";
  $query = "SELECT * FROM log_requests ORDER BY id DESC LIMIT 5 ;";
  $result = $conn->query($query);
  if($result){
    //return last 5 log requests in terminal
    while($row = $result->fetch_assoc()) {
         echo "===============================";
         echo "\n id: " . $row["id"]. "\n first int: " . $row["integer_1"]. "\n second int :" . $row["integer_2"]. "\n average :" . $row["average"] ."\n area :". $row["area"] . "\n area^2 :" . $row["area_square"]."\n";
         $html_columns.="<tr><td>".$row["id"]."</td><td>".$row["integer_1"]."</td><td>".$row["integer_2"]."</td><td>".$row["average"]."</td><td>".$row["area"]."</td><td>".$row["area_square"]."</td></tr>";
     }
     //call file generating
     echo html_generator($insertion_id , $html_columns);
  }
} else {
    echo "Error: " . $sql_insert . "<br>" . $conn->error;
}
