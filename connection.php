<?php

// Connect to MySQL
$conn = mysqli_connect($serverIp, $username, $password);
if ($conn->connect_error) {
    die("connecting to server failed: " . $conn->connect_error);
}

// check db existance
if (!mysqli_select_db($conn,$dbName)){
  $create_db = "CREATE DATABASE IF NOT EXISTS `test_bytology` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
  $use_db = "USE `test_bytology`;";
  $create_table = "  CREATE TABLE IF NOT EXISTS `log_requests` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `integer_1` int(11) NOT NULL,
      `integer_2` int(11) NOT NULL,
      `average` float NOT NULL,
      `area` int(11) NOT NULL,
      `area_square` int(11) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;";
  if ($conn->query($create_db) === TRUE && $conn->query($use_db) === TRUE && $conn->query($create_table) === TRUE) {
      echo "Database created successfully with the name newDB\n";
  } else {
      die("Error creating database: " . $conn->error);
  }
}
echo "********** Connected to db successfully **********\n";
